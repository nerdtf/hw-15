<?php

class Food extends Item
{
    static public function getType(){
        return 'food';
    }

    public function getPrice()
    {
       $discount =  $this->price * 0.9;
       return $discount;
    }

    public function getSummaryLine()
    {
        $str = '';
        $str = self::getTitle() . ' Price: ' . self::getPrice() . '<br>';
        echo $str;

    }
}