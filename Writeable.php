<?php
interface Writeable{
    public function getSummaryLine();
}