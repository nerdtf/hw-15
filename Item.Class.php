<?php

abstract class Item implements Writeable
{
    protected $type = '';
    protected $title ='';
    protected $price = 0;



    public function getTitle()
    {
        return $this->title;
    }

    static public function getType(){
        return 'base_item';
    }


    public function __construct($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
        $this->type = self::getType();
    }

    abstract public function getPrice();



}