<?php


class GoodsItem extends Item
{
    protected $discount = 0;

    public function __construct($title, $price , $discount)
    {
        parent::__construct($title, $price);
        $this->discount = $discount;
    }

    static public function getType(){
        return 'goods';
    }

    public function getPrice()
    {
        $newPrice = $this->price - $this->discount;
        return $newPrice;
    }

    public function getSummaryLine()
    {
        $str = '';
        $str = self::getTitle() . ' Price: ' . self::getPrice(). '<br>';
        echo $str;
    }


}