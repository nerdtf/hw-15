<?php
require_once 'Writeable.php';
require_once 'Item.Class.php';
require_once 'Food.Class.php';
require_once 'GoodsItem.Class.php';
require_once 'Bonus.Class.php';
require_once 'ItemsWriter.Class.php';

$foodItem = new Food('Onion', 100);
$goodsItem = new GoodsItem('Phone', 800 , 30);
$bonusItem = new Bonus('Tokens', 'Play and get money');
$itemWriter = new ItemsWriter();
$itemWriter->addItem($foodItem);
$itemWriter->addItem($goodsItem);
$itemWriter->addItem($bonusItem);
$itemWriter->write();




