<?php

class ItemsWriter
{
    protected $items = [];
    public function addItem(Writeable $obj){
        $this->items[] = $obj;
    }
    public function write(){
        $str = '';
        foreach ($this->items as $item){
            $str .= $item->getSummaryLine();
        }
        echo $str;

    }

}