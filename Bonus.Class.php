<?php


class Bonus implements Writeable
{
    protected $title = '';
    protected $type = '';
    protected $description = '';


    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
        $this->type = self::getType();
    }


    public function getTitle()
    {
        return $this->title;
    }


    public function getDescription()
    {
        return $this->description;
    }



    static public function getType(){
        return 'Bonus';
    }

    public function getSummaryLine()
    {
        $str = '';
        $str = self::getTitle() . ' Description: ' . self::getDescription(). '<br>';
        echo $str;
    }


}